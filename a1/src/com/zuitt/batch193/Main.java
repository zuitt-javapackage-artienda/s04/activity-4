package com.zuitt.batch193;

public class Main {

    public static void main (String[]args){
        
        User instructor1 = new User("Kim", "Artienda", 27, "Mandaluyong");

        System.out.println(instructor1.getFirstName());
        System.out.println(instructor1.getLastName());
        System.out.println(instructor1.getAge());
        System.out.println(instructor1.getAddress());

        Course course1 = new Course();
        course1.setName("Introduction of Java");
        course1.setDescription("This course will discuss about fundamentals of Java");
        course1.setSeat(20);
        course1.setFee(50000.50);
        course1.setStartDate("July 1, 2022");
        course1.setEndDate("December 20, 2022");
        course1.setInstructor(instructor1);

        System.out.println(course1.getName());
        System.out.println(course1.getDescription());
        System.out.println(course1.getFee());
        System.out.println(course1.getInstructor().getFirstName());





    }
}
